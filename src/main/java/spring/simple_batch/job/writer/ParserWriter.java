package spring.simple_batch.job.writer;

import spring.simple_batch.model.CDInfo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ParserWriter implements ItemWriter<CDInfo> {
    private final Logger logger = LoggerFactory.getLogger(ParserWriter.class);

    @Value("#{jobParameters['file.path']}")
    private String filePath;

    public void write(final List<? extends CDInfo> list) throws Exception {
        list.forEach(i -> {
            try {
                FileUtils.writeStringToFile(new File(filePath), i.toString() + "\n", true);
            } catch (IOException e) {
                logger.error("Error on writing CD entries");
                throw new WriterExeption();
            }
        });
    }
}
