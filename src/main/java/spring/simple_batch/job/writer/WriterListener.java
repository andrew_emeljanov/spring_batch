package spring.simple_batch.job.writer;

import spring.simple_batch.model.CDInfo;
import org.springframework.batch.core.ItemWriteListener;

import java.util.List;

public class WriterListener implements ItemWriteListener<CDInfo> {

    public void beforeWrite(List<? extends CDInfo> list) {

    }

    public void afterWrite(List<? extends CDInfo> list) {

    }

    public void onWriteError(Exception e, List<? extends CDInfo> list) {

    }
}
