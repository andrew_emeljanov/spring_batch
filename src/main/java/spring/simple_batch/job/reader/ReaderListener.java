package spring.simple_batch.job.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemReadListener;
import org.w3c.dom.Node;

public class ReaderListener implements ItemReadListener<Node> {
    private final Logger logger = LoggerFactory.getLogger(ReaderListener.class);

    public void beforeRead() {
    }

    public void afterRead(Node node) {
        logger.info("Line with {} symbols has been read", node.getTextContent().length());
    }

    public void onReadError(Exception e) {

    }
}
