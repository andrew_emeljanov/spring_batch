package spring.simple_batch.job.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import spring.simple_batch.utils.PageReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

public class ParserReader implements ItemReader<Node> {
    private final Logger logger = LoggerFactory.getLogger(ParserReader.class);

    private final static String TAG_NAME = "CD";

    @Autowired
    private PageReader pageReader;

    @Value("#{jobParameters['url']}")
    private String url;

    private NodeList nodeList;

    private int cursor = 0;

    private void readPage() {
        String content = pageReader.getPageContent(url);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(content)));
            nodeList = doc.getElementsByTagName(TAG_NAME);
        } catch (Exception e) {
            logger.error("Error on reading CD entries");
            throw new ReaderException();
        }
    }

    public Node read() throws Exception {
        if(nodeList == null) {
            readPage();
        }
        if(nodeList.getLength() != 0) {
            return nodeList.item(cursor++);
        }
        return null;
    }
}
