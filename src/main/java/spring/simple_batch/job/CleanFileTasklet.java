package spring.simple_batch.job;

import spring.simple_batch.job.writer.WriterExeption;
import org.apache.commons.io.FileUtils;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.IOException;

public class CleanFileTasklet implements Tasklet {

    @Value("#{jobParameters['file.path']}")
    private String filePath;

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        File file = new File(filePath);
        if(file.exists()) {
            try {
                FileUtils.writeStringToFile(file, "");
            } catch (IOException e) {
                throw new WriterExeption();
            }
        }
        return RepeatStatus.FINISHED;
    }
}
