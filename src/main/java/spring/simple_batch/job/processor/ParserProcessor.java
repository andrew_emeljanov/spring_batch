package spring.simple_batch.job.processor;

import spring.simple_batch.model.CDInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.w3c.dom.Node;

public class ParserProcessor implements ItemProcessor<Node, CDInfo> {
    private final Logger logger = LoggerFactory.getLogger(ParserProcessor.class);

    public CDInfo process(Node xmlNode) throws Exception {
        CDInfo CDInfo = new CDInfo();
        try {
            CDInfo.setTitle(xmlNode.getChildNodes().item(1).getTextContent());
            CDInfo.setArtist(xmlNode.getChildNodes().item(3).getTextContent());
            CDInfo.setCountry(xmlNode.getChildNodes().item(5).getTextContent());
            CDInfo.setCompany(xmlNode.getChildNodes().item(7).getTextContent());
            CDInfo.setPrice(xmlNode.getChildNodes().item(9).getTextContent());
            CDInfo.setYear(xmlNode.getChildNodes().item(11).getTextContent());
        } catch (Exception e) {
            logger.error("Error on processing CD entries");
            throw new ProcessorException();
        }
        return CDInfo;
    }
}
