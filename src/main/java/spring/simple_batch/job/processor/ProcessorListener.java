package spring.simple_batch.job.processor;

import spring.simple_batch.model.CDInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemProcessListener;
import org.w3c.dom.Node;

public class ProcessorListener implements ItemProcessListener<Node, CDInfo> {
    private final Logger logger = LoggerFactory.getLogger(ProcessorListener.class);

    public void beforeProcess(Node node) {

    }

    public void afterProcess(Node node, CDInfo CDInfo) {
        logger.info("Item {} has been processed", CDInfo);
    }

    public void onProcessError(Node node, Exception e) {

    }
}
