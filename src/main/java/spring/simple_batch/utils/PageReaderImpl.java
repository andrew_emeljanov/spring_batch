package spring.simple_batch.utils;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

public class PageReaderImpl implements PageReader {
    private HttpClient client = HttpClientBuilder.create().build();

    public String getPageContent(String url) throws PageReaderException {
        HttpGet get = new HttpGet(url);
        try {
            return IOUtils.toString(client.execute(get).getEntity().getContent());
        } catch (IOException e) {
            throw new PageReaderException();
        }
    }
}
