package spring.simple_batch.utils;

public interface PageReader {
    String getPageContent(String url) throws PageReaderException;
}
