package spring.simple_batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {

    public static void main(String ...args) {
        if(args.length == 0) throw new RuntimeException("Need to specify folder");
        String[] springConfig  =
                {
                        "spring/context.xml",
                        "spring/job.xml"
                };

        ApplicationContext context =
                new ClassPathXmlApplicationContext(springConfig);

        JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
        Job job = (Job) context.getBean("parserJob");

        try {
            JobParameters jobParameters = new JobParametersBuilder()
                    .addString("file.path", args[0])
                    .addString("url", "http://www.w3schools.com/xml/cd_catalog.xml")
                    .toJobParameters();

            JobExecution execution = jobLauncher.run(job, jobParameters);
            System.out.println("Exit Status : " + execution.getStatus());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
